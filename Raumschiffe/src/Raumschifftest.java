import java.util.ArrayList;

public class Raumschifftest {

	public static void main(String[] args) {
		Raumschiff klingonen = new Raumschiff(1,100,100,100,100,"IKS Hegh'Ta",2);
		Raumschiff romulaner = new Raumschiff(2,100,100,100,100,"IRW Khazara",2);
		Raumschiff vulkanier = new Raumschiff(0,80,80,50,100,"Ni'Var",5);
		
		Ladung klingonenLadung1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung klingonenLadung2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		
		Ladung romulanerLadung1 = new Ladung("Borg-Schrott", 5);
		Ladung romulanerLadung2 = new Ladung("Plasma-Waffe", 50);
		Ladung romulanerLadung3 = new Ladung("Rote Materie", 2);
		
		Ladung vulkanierLadung1 = new Ladung("Forschungssonde", 35);
		Ladung vulkanierLadung2 = new Ladung("Photonentorpedo", 3);
		
		klingonen.addLadung(klingonenLadung1);
		klingonen.addLadung(klingonenLadung2);
		
		romulaner.addLadung(romulanerLadung1);
		romulaner.addLadung(romulanerLadung2);
		romulaner.addLadung(romulanerLadung3);
		
		vulkanier.addLadung(vulkanierLadung1);
		vulkanier.addLadung(vulkanierLadung2);
		
		// Methoden:
		
		klingonen.photonentorpedoSchiessen(romulaner);
		romulaner.phaserkanoneSchiessen(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");  
		klingonen.Raumschiffzustand();
		klingonen.ladungsverzeichnisAusgeben();
		vulkanier.reparaturDurchfuehren(true, true, true, vulkanier.getAndroidenAnzahl());
		vulkanier.photonentorpedosLaden(3);
		vulkanier.ladungsverzeichnisAufraeumen();
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		
		klingonen.Raumschiffzustand();	
		klingonen.ladungsverzeichnisAusgeben();
		romulaner.Raumschiffzustand();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.Raumschiffzustand();
		vulkanier.ladungsverzeichnisAusgeben();
		
		// Broadcast Kommunikator
		System.out.println();
		// Alle Eintr�ge in einem logbuch
//	 	ArrayList<String> logbuch = new ArrayList<String>();
//		logbuch.addAll(vulkanier.eintraegeLobguchZurueckgeben());
//		logbuch.addAll(klingonen.eintraegeLobguchZurueckgeben());
//		logbuch.addAll(romulaner.eintraegeLobguchZurueckgeben());
//		
//		for (String s : logbuch) {
//			System.out.println(s);
//		}
		
		vulkanier.logbuchAusgeben(vulkanier.eintraegeLobguchZurueckgeben());
		romulaner.logbuchAusgeben(romulaner.eintraegeLobguchZurueckgeben());
		klingonen.logbuchAusgeben(klingonen.eintraegeLobguchZurueckgeben());
	}
}