
public class Ladung {

    private String bezeichnung;	//Attribute
    private int menge;

    public Ladung() {

    }

    public Ladung (String bezeichnung, int menge) {
        this.bezeichnung = bezeichnung;
        this.menge = menge;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public int getMenge() {
        return menge;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }


    @Override
    /**
     * Liefert eine Zeichenkette mit Info des ak. Objektes
     * @return Gibt einen String zur�ck
     */
    public String toString() {
        return "Type: " + this.getBezeichnung() + 
               ", Menge: " + this.getMenge();
    }
}


