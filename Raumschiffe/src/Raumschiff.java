import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
/**
 * @author Hende�
 */
public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private String schiffname;
	private int androidenAnzahl;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	// Konstruktoren

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, String schiffname, int androidenAnzahl) {

		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffname = schiffname;
	}

	// Getter und Setter

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffname() {
		return schiffname;
	}

	public void setSchiffname(String schiffname) {
		this.schiffname = schiffname;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	// Methoden
	/**
	 * 
	 * @param neueLadung Bei dieser Methode wird dem ladungsverzeichnis eine neue Ladung hinzugef�gt
	 */

	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}
	/**
	 * 
	 * @param r Das getroffene Raumschiff wird and die Methode �bergeben
	 */
	public void photonentorpedoSchiessen(Raumschiff r) {
		if (this.photonentorpedoAnzahl < 1) {
			this.nachrichtAnAlle("=*Click*=-");
		} else {
			this.photonentorpedoAnzahl = this.photonentorpedoAnzahl - 1;
			this.nachrichtAnAlle("Photonentorpedo abgeschossen");
			this.treffer(r);
		}
	}
	/**
	 * 
	 * @param r Das getroffene Raumschiff wird �bergeben und die Energieversorgung nimmt ab
	 */
	public void phaserkanoneSchiessen(Raumschiff r) {
		if (this.energieversorgungInProzent < 50) {
			this.nachrichtAnAlle("=*Click*=");
		} else {
			this.energieversorgungInProzent -= 50;
			this.nachrichtAnAlle("Phaserkanone abgeschossen");
			this.treffer(r);
		}
	}
	/**
	 * 
	 * @param r Das zu �berwachende Raumschiff wird zur Bearbeitung und Ausgabe �bergeben
	 */
	private void treffer(Raumschiff r) {
		System.out.println(r.schiffname + " wurde getroffen!");
		// Treffer vermerken
		r.schildeInProzent -= 50;
		if (r.schildeInProzent <= 0) {
			r.schildeInProzent = 0;
			r.huelleInProzent -= 50;
			r.energieversorgungInProzent -= 50;
		}

		if (r.huelleInProzent <= 0) {
			r.huelleInProzent = 0;
			r.lebenserhaltungssystemeInProzent = 0;
			this.nachrichtAnAlle("Die Lebenserhaltungssysteme von " + r.schiffname + " wurden vernichtet.");
		}
	}
	/**
	 * 
	 * @param message Die Nachricht, die an alle gesendet werden soll wird �bergeben
	 */
	public void nachrichtAnAlle(String message) {
		System.out.println("\nNachricht an Alle:");
		System.out.println("\t" + message);
		this.broadcastKommunikator.add(message);
	}

	public ArrayList<String> eintraegeLobguchZurueckgeben() {
		return this.broadcastKommunikator;
	}
	/**
	 * 
	 * @param logbuch Das auszugebende Logbuch wird �bergeben
	 */
	public void logbuchAusgeben(ArrayList<String> logbuch) {
		System.out.printf("\n%s\"%s\":\n", "Logbuch: ", this.schiffname);
		for (String s : logbuch) {
			System.out.println("\t" + s);
		}
	}
	/**
	 * 
	 * @param anzahlTorpedos In dieser Methode wird die photonentorpedoanzahl ver�ndert
	 */
	public void photonentorpedosLaden(int anzahlTorpedos) {
		boolean check = false;

		for (int i = 0; i < this.getLadungsverzeichnis().size(); i++) {
			if (this.getLadungsverzeichnis().get(i).getBezeichnung().toLowerCase().equals("photonentorpedo")) {
				if (this.getLadungsverzeichnis().get(i).getMenge() >= anzahlTorpedos) {
					check = true;
					this.photonentorpedoAnzahl = anzahlTorpedos;
					int neueMenge = this.getLadungsverzeichnis().get(i).getMenge() - anzahlTorpedos;
					this.getLadungsverzeichnis().get(i).setMenge(neueMenge);
					System.out.println(this.photonentorpedoAnzahl + " Photonentorpedo(s) eingesetzt");
					break;
				}
			}

		}

		if (check) {
		} else {
			System.out.println("Keine Photonentorpedos gefunden!");
			this.nachrichtAnAlle("-=*Click*=-");
		}
	}
	/**
	 * 
	 * @param schutzschilde	Der Boolean wird �berpr�ft und muss daher �bergeben werden
	 * @param energieversorgung	Der Boolean wird �berpr�ft und muss daher �bergeben werden
	 * @param schiffshuelle Der Boolean wird �berpr�ft und muss daher �bergeben werden
	 * @param anzahlDroiden Der Boolean wird �berpr�ft und muss daher �bergeben werden
	 */
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int anzahlDroiden) {

		int zuReparieren = 0;
		if (schutzschilde)
			zuReparieren++;
		if (energieversorgung)
			zuReparieren++;
		if (schiffshuelle)
			zuReparieren++;

		// Zufallszahl generieren
		Random random = new Random();
		int r = random.nextInt(100 + 1) + 1;

		if (anzahlDroiden > this.androidenAnzahl) {
			anzahlDroiden = this.androidenAnzahl;
		}

		if (zuReparieren != 0) {
			double reparaturInProzent = (r * anzahlDroiden) / zuReparieren;

			if (schutzschilde)
				this.schildeInProzent += reparaturInProzent;
			if (energieversorgung)
				this.energieversorgungInProzent += reparaturInProzent;
			if (schiffshuelle)
				this.huelleInProzent += reparaturInProzent;
		}

	}

	public void Raumschiffzustand() {
		System.out.println("\nSchiffsnamen: " + this.schiffname);
		System.out.println("Energieversorung: " + this.energieversorgungInProzent + "%");
		System.out.println("Anzahl Photonentorpedos: " + this.photonentorpedoAnzahl);
		System.out.println("Schilde: " + this.schildeInProzent + "%");
		System.out.println("H�lle: " + this.huelleInProzent + "%");
		System.out.println("Lebenserhaltungssysteme: " + this.lebenserhaltungssystemeInProzent + "%");
		System.out.println("Anzahl Androiden: " + this.androidenAnzahl);
	}

	public void ladungsverzeichnisAusgeben() {
		System.out.println("\nLadungsverzeichnis: ");
		for (Ladung ladung : this.getLadungsverzeichnis()) {
			System.out.println("\t" + ladung);
		}
	}

	public void ladungsverzeichnisAufraeumen() {
		for (int i = 0; i < this.getLadungsverzeichnis().size(); i++) {
			if (this.getLadungsverzeichnis().get(i).getMenge() == 0) {
				this.getLadungsverzeichnis().remove(i);
			}
		}
	}

}