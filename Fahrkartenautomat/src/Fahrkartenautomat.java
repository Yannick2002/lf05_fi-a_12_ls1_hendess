import java.util.Scanner;

public class Fahrkartenautomat {
	public static void main(String[] args) {
		while (true) {
		double nochzuZahlenderBetrag = fahrkartenbestellungErfassen();
		// Geldeinwurf
		// -----------
		double eingezahlterGesamtbetrag = fahrkartenBezahlen(nochzuZahlenderBetrag);
		// Fahrscheinausgabe
		// -----------------
		fahrkartenAusgeben();
		// R�ckgeldberechnung und -Ausgabe
		// -------------------------------
		rueckgeldAusgeben(eingezahlterGesamtbetrag, nochzuZahlenderBetrag);
		
		}
	}
	public static double fahrkartenbestellungErfassen() {
		int anzahlderTickets=0;
		double zuZahlenderBetrag = 0;
		double nochzuZahlenderBetrag = 0;
		int ticketwahl =1;
		
		String[] ticketwaehlen= {"Einzelfahrschein Berlin AB ", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", 
				"Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", 
				"Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
		
		double preis[] = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
        System.out.println("----------------------------------------------------------------------------------");

		for(int i = 0; i<ticketwaehlen.length; i++) {
			
            System.out.printf("%d \t\t\t%-34s \t\t%.2f\n", i+1, ticketwaehlen[i], preis[i]);

		}
        System.out.println("----------------------------------------------------------------------------------");

		
		while(ticketwahl!=0) {

			Scanner tastatur = new Scanner(System.in);
			System.out.print("W�hlen Sie ihre Wunschfahrkarte aus:\r\n"
					+ "Um zu bezahlen, dr�cken Sie die 0: ");
			ticketwahl = tastatur.nextInt();
			if(ticketwahl == 0) {
				break;
			}
			while(ticketwahl <0||ticketwahl >10) {
				System.out.println("Entschuldigen Sie gn�dige Person. Die von Ihnen gew�hlte Auswahl ist nicht verf�gbar ");
				System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:\r\n"+ "Um zu bezahlen, dr�cken Sie die 0: ");
				

			        System.out.println("----------------------------------------------------------------------------------");
			        for(int j = 0; j<ticketwaehlen.length;j++) {
			            System.out.printf("%d \t\t\t%-34s \t\t%.2f\n", j+1, ticketwaehlen[j], preis[j]);
			            }
		            System.out.println("----------------------------------------------------------------------------------");
					

				ticketwahl = tastatur.nextInt();		}
				zuZahlenderBetrag = preis[ticketwahl-1];
			
			
		Scanner tickets = new Scanner(System.in);
		System.out.println("Geben Sie die Anzahl der Tickets ein: ");
			anzahlderTickets = tickets.nextInt();		//Ganzzahl --> Integer
		while(anzahlderTickets>10||anzahlderTickets<0) {
			System.out.println("Maximal 10 Tickets ");
			System.out.println("Geben Sie die Anzahl der Tickets ein: ");
			anzahlderTickets = tickets.nextInt();		//Ganzzahl --> Integer
			
		}
		nochzuZahlenderBetrag = zuZahlenderBetrag * anzahlderTickets;	//Dezimalzahl --> double, der Ticketpreis wird mit der Anzahl der Tickets multipliziert
		}
		return nochzuZahlenderBetrag;
		
	}
	public static double fahrkartenBezahlen(double nochzuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < nochzuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: " + "%.2f", (nochzuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
			double eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		return eingezahlterGesamtbetrag;
	
	}
	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(500);

		}
		System.out.println("\n\n");
	}
	public static void warte(int millisekunden) {
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double nochzuZahlenderBetrag) {
		double eingeworfeneM�nze;			// Alles Dezimalzahlen --> double		
		double r�ckgabebetrag = eingezahlterGesamtbetrag - nochzuZahlenderBetrag;
		if (r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von " + "%.2f", r�ckgabebetrag);
			System.out.println(" Euro");
			System.out.println("wird in folgenden M�nzen ausgezahlt:");
			while (r�ckgabebetrag >= 1.99) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				muenzeAusgeben(2, "Euro");
				r�ckgabebetrag -= 2.0;
			}
			while (r�ckgabebetrag >= 0.99) // 1 EURO-M�nzen
			{
				muenzeAusgeben(1, "Euro");
				r�ckgabebetrag -= 1.0;
			}
			while (r�ckgabebetrag >= 0.49) // 50 CENT-M�nzen
			{
				muenzeAusgeben(50, "Cent");
				r�ckgabebetrag -= 0.5;
			}
			while (r�ckgabebetrag >= 0.19) // 20 CENT-M�nzen
			{
				muenzeAusgeben(20, "Cent");
				r�ckgabebetrag -= 0.2;
			}
			while (r�ckgabebetrag >= 0.09) // 10 CENT-M�nzen
			{
				muenzeAusgeben(10, "Cent");
				r�ckgabebetrag -= 0.1;
			}
			while (r�ckgabebetrag >= 0.04)// 5 CENT-M�nzen
			{
				muenzeAusgeben(5, "Cent");
				r�ckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");		
	}
	public static void muenzeAusgeben(int rueckgabebetrag, String einheit) {
        System.out.printf("   ***\n *      * \n*   %-2s   * \n*  %s  * \n *      * \n   ***\n", rueckgabebetrag, einheit);
    }
}


