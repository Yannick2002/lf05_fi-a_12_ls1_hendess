import java.util.Scanner;
public class PCHaendler {

	public static void main(String[] args) {
		
			String artikel = liesString("Was moechten Sie bestellen?");
			int anzahl = liesInt("Geben Sie die Anzahl ein:");
			double preis = liesDouble("Geben Sie den Nettopreis ein:");
			double mwst = liesMwst("Geben Sie den Mehrwertsteuersatz in Prozent ein:");

			double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
			double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
			ausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

		}
	public static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		String artikel = myScanner.next();
		return artikel;
	}
	public static int liesInt(String txt) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(txt);
		int anzahl = myScanner.nextInt();
		return anzahl;
	}
	public static double liesDouble(String txt) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(txt);
		double preis = myScanner.nextDouble();
		return preis;
	}
	public static double liesMwst(String txt) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(txt);
		double mwst = myScanner.nextDouble();
		return mwst;
	}
	public static double berechneGesamtnettopreis(int anzahl, double preis) {
		double bruttogesamtpreis = anzahl * preis;
		return bruttogesamtpreis;
	}
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double nettopreis = nettogesamtpreis * (1 + mwst / 100);
		return nettopreis;
	}
	public static void ausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}

	}
