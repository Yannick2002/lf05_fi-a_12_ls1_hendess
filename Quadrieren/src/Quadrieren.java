
public class Quadrieren {

	public static void main(String[] args) {
		double x = 5;
		eingabe();
		double ergebnis = verarbeitung(x);
		ausgabe(ergebnis, x);
		// (E) "Eingabe"
		// Wert f�r x festlegen:
		// ===========================

		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================

	}

	public static void eingabe() {
		System.out.println("Dieses Programm berechnet die Quadratzahl x�");
		System.out.println("---------------------------------------------");
	}

	public static double verarbeitung(double x) {
		double ergebnis = x * x;
		return ergebnis;
	}

	public static void ausgabe(double ergebnis, double x) {
		System.out.printf("x = %.2f und x�= %.2f\n", x, ergebnis);
	}
}
