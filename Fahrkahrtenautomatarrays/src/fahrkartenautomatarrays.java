import java.util.Scanner;

public class fahrkartenautomatarrays {
	public static void main(String[] args) {
		while (true) {
		double nochzuZahlenderBetrag = fahrkartenbestellungErfassen();
		// Geldeinwurf
		// -----------
		double eingezahlterGesamtbetrag = fahrkartenBezahlen(nochzuZahlenderBetrag);
		// Fahrscheinausgabe
		// -----------------
		fahrkartenAusgeben();
		// R�ckgeldberechnung und -Ausgabe
		// -------------------------------
		rueckgeldAusgeben(eingezahlterGesamtbetrag, nochzuZahlenderBetrag);
		}
	}
	public static double fahrkartenbestellungErfassen() {
		int anzahlderTickets=0;
		double zuZahlenderBetrag = 0;
		double nochzuZahlenderBetrag = 0;
		int ticketwahl =0;
		while(ticketwahl!=9) {

			Scanner tastatur = new Scanner(System.in);
			System.out.print("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:\r\n"
					+ "  Einzelfahrschein Regeltarif AB [3,00 EUR] (1)\r\n"
					+ "  Tageskarte Regeltarif AB [8,80 EUR] (2)\r\n"
					+ "  Kleingruppen-Tageskarte Regeltarif AB [25,50 EUR] (3)\r\n"
					+ "Bezahlen (9)");
			ticketwahl = tastatur.nextInt();
			if(ticketwahl == 9) {
				break;
			}
			while(ticketwahl <0||ticketwahl >3) {
				System.out.println("Entschuldigen Sie gn�dige Person. Die von Ihnen gew�hlte Auswahl ist nicht verf�gbar ");
				System.out.print("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:\r\n"
						+ "  Einzelfahrschein Regeltarif AB [3,00 EUR] (1)\r\n"
						+ "  Tageskarte Regeltarif AB [8,80 EUR] (2)\r\n"
						+ "  Kleingruppen-Tageskarte Regeltarif AB [25,50 EUR] (3)");
				ticketwahl = tastatur.nextInt();		}
			if(ticketwahl == 1) {
				zuZahlenderBetrag = 3.00;
			}
			else if(ticketwahl == 2) {
				zuZahlenderBetrag = 8.80;
			}
			else if(ticketwahl == 3) {
				zuZahlenderBetrag = 25.50;
			}
			
		Scanner tickets = new Scanner(System.in);
		System.out.println("Geben Sie die Anzahl der Tickets ein: ");
			anzahlderTickets = tickets.nextInt();		//Ganzzahl --> Integer
		while(anzahlderTickets>10||anzahlderTickets<0) {
			System.out.println("Maximal 10 Tickets ");
			System.out.println("Geben Sie die Anzahl der Tickets ein: ");
			anzahlderTickets = tickets.nextInt();		//Ganzzahl --> Integer
			
		}
		nochzuZahlenderBetrag = zuZahlenderBetrag * anzahlderTickets;	//Dezimalzahl --> double, der Ticketpreis wird mit der Anzahl der Tickets multipliziert
		}
		return nochzuZahlenderBetrag;
		
	}
	public static double fahrkartenBezahlen(double nochzuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < nochzuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: " + "%.2f", (nochzuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
			double eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		return eingezahlterGesamtbetrag;
	
	}
	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(500);

		}
		System.out.println("\n\n");
	}
	public static void warte(int millisekunden) {
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double nochzuZahlenderBetrag) {
		double eingeworfeneM�nze;			// Alles Dezimalzahlen --> double		
		double r�ckgabebetrag = eingezahlterGesamtbetrag - nochzuZahlenderBetrag;
		if (r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von " + "%.2f", r�ckgabebetrag);
			System.out.println(" Euro");
			System.out.println("wird in folgenden M�nzen ausgezahlt:");
			while (r�ckgabebetrag >= 1.99) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				r�ckgabebetrag -= 2.0;
			}
			while (r�ckgabebetrag >= 0.99) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				r�ckgabebetrag -= 1.0;
			}
			while (r�ckgabebetrag >= 0.49) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				r�ckgabebetrag -= 0.5;
			}
			while (r�ckgabebetrag >= 0.19) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				r�ckgabebetrag -= 0.2;
			}
			while (r�ckgabebetrag >= 0.09) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				r�ckgabebetrag -= 0.1;
			}
			while (r�ckgabebetrag >= 0.04)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				r�ckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");		
	}
	public static void muenzeAusgeben(int betrag, String einheit) {
		
	}
}


