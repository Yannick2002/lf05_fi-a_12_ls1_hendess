import java.util.Scanner;

public class Variablen {

	public static void main(String[] args) {
	    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
        Vereinbaren Sie eine geeignete Variable */
	  int zaehler = 25;
	  System.out.println(zaehler);
	  

        /* 2. Weisen Sie dem Zaehler den Wert 25 zu
        und geben Sie ihn auf dem Bildschirm aus.*/
	  	Scanner eingabe = new Scanner(System.in);
	  	System.out.println("Gib einen Buchstaben ein: ");
	  	String menue = eingabe.next();

	  	/* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
        eines Programms ausgewaehlt werden.
        Vereinbaren Sie eine geeignete Variable */
	  	String i = "C";
	  	System.out.println(i);
	 /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
        und geben Sie ihn auf dem Bildschirm aus.*/

	 /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
        notwendig.
        Vereinbaren Sie eine geeignete Variable */
	  
	  final int lichtges = 299792458;
	  System.out.println("Die Lichtgeschwindigkeit betr�gt " + lichtges + " m/s");
			  
	/* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
        und geben Sie sie auf dem Bildschirm aus.*/

    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
        soll die Anzahl der Mitglieder erfasst werden.
        Vereinbaren Sie eine geeignete Variable und initialisieren sie
        diese sinnvoll.*/
	  short anzahlMitglieder = 7;
	  System.out.println("Der Verein hat " + anzahlMitglieder + " Mitglieder.");

    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/

    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
        Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
        dem Bildschirm aus.*/
	  double elekElementarladung = 0.00000000000000000016;
	  System.out.println(elekElementarladung);
	  
    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
        Vereinbaren Sie eine geeignete Variable. */
	  boolean zahlungerfolgt;
	  zahlungerfolgt = true;
	  System.out.println(zahlungerfolgt);
    /*11. Die Zahlung ist erfolgt.
        Weisen Sie der Variable den entsprechenden Wert zu
        und geben Sie die Variable auf dem Bildschirm aus.*/

	}

}
