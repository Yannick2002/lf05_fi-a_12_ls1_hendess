
public class konsolenausgabe2 {

	public static void main(String[] args) { 		//Yannick Hende�
//		String s = "*";
//		System.out.printf("%6s\n", s + s);
//		System.out.printf("%3s" , s);
//		System.out.printf("%5s\n", s);
//		System.out.printf("%3s" , s);
//		System.out.printf("%5s\n", s);
//		System.out.printf("%6s\n", s + s);
//		Aufgabe2();
		Tabelle();  //Aufgabe 3
	}

	public static void Tabelle() {    		//Aufgabe 3
		System.out.printf("%-12s", "Fahrenheit");
		System.out.print("|");
		System.out.printf("%10s \n", "Celsius");
		System.out.printf("------------------------ \n");

		System.out.printf("%-12d|%10.2f \n", -20, -28.89);
		System.out.printf("%-12d|%10.2f \n", -10, -23.33);
		System.out.printf("%-12d|%10.2f \n", 0, -17.78);
		System.out.printf("%-12d|%10.2f \n", 10, -6.67);
		System.out.printf("%-12d|%10.2f \n", 20, -1.11);
	}

	public static void Aufgabe2() {
		System.out.print("0!");
		System.out.printf("%5s", "=");
		System.out.printf("%19s", "");
		System.out.print("=");
		System.out.printf("%5d \n", 1);

		System.out.print("1!");
		System.out.printf("%5s", "=");
		System.out.printf("%-19s", " 1");
		System.out.print("=");
		System.out.printf("%5d \n", 1);

		System.out.print("2!");
		System.out.printf("%5s", "=");
		System.out.printf("%-19s", " 1*2");
		System.out.print("=");
		System.out.printf("%5d \n", 2);

		System.out.print("3!");
		System.out.printf("%5s", "=");
		System.out.printf("%-19s", " 1*2*3");
		System.out.print("=");
		System.out.printf("%5d \n", 6);

		System.out.print("4!");
		System.out.printf("%5s", "=");
		System.out.printf("%-19s", " 1*2*3*4");
		System.out.print("=");
		System.out.printf("%5d \n", 24);

		System.out.print("5!");
		System.out.printf("%5s", "=");
		System.out.printf("%-19s", " 1*2*3*4*5");
		System.out.print("=");
		System.out.printf("%5d \n", 120);
	}
}
