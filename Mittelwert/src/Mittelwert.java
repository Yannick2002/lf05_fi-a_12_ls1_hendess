import java.util.Scanner;
public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
      double x = liesDouble("Geben Sie einen Double ein: ");
      double y = liesDouble("Geben Sie noch einen Double ein: ");;
      double m;
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      m = mittelwert(x, y);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================

      mittelwertAusgeben(x,y,m);
      
   }
   public static double mittelwert (double wert1, double wert2) {
	   double erg = (wert1 + wert2) / 2;
	   return erg;
   }
   public static void mittelwertAusgeben(double wert1, double wert2, double mittelwert) {
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", wert1, wert2, mittelwert);
   }
	public static double liesDouble(String txt) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(txt);
		double wert = myScanner.nextDouble();
		return wert;
	}
}

